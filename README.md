# Introduction to accessibility in web-development

## Prerequisites

To run the demo you have to have node installed. You can install it from
[NodeJS.org](https://nodejs.org/en/download/) or via nvm (node version manager)
for [mac](https://github.com/nvm-sh/nvm) or [windows](https://github.com/coreybutler/nvm-windows)

After installing node you need to run the following command in this folder.

`npm install`

The presentation is written in `org-mode` which is supported in emacs (built in) and to some
extend in vs-code via [the vs-code-org-mode plugin](https://github.com/vscode-org-mode/vscode-org-mode)

## Running the demo

To run the actually demo run the following command

`npm start`

This will spin up the demo on http://localhost:1234 (or if you are already
running something on that port it will show another url in the terminal)

## Links

### A11y cast
The actual channel https://www.youtube.com/playlist?list=PLNYkxOF6rcICWx0C9LVWWVqvHlYJyqw7g

Screen reader basics:
[VoiceOver (mac)](https://www.youtube.com/watch?v=5R-6WvAihms&list=PLNYkxOF6rcICWx0C9LVWWVqvHlYJyqw7g&index=25&t=0s)
[NVDA (windows)](https://www.youtube.com/watch?v=Jao3s_CwdRU&list=PLNYkxOF6rcICWx0C9LVWWVqvHlYJyqw7g&index=23&t=0s)

### Extensions
#### Code sniffer
This is actually a small bookmarklet that you activtate on a given page and it
is a quick and easy way to get some information.

javascript:(function() %7Bvar _p%3D%27http://squizlabs.github.io/HTML_CodeSniffer/build/%27%3Bvar _i%3Dfunction(s,cb) %7Bvar sc%3Ddocument.createElement(%27script%27)%3Bsc.onload %3D function() %7Bsc.onload %3D null%3Bsc.onreadystatechange %3D null%3Bcb.call(this)%3B%7D%3Bsc.onreadystatechange %3D function()%7Bif(/%5E(complete%7Cloaded)%24/.test(this.readyState) %3D%3D%3D true)%7Bsc.onreadystatechange %3D null%3Bsc.onload()%3B%7D%7D%3Bsc.src%3Ds%3Bif (document.head) %7Bdocument.head.appendChild(sc)%3B%7D else %7Bdocument.getElementsByTagName(%27head%27)%5B0%5D.appendChild(sc)%3B%7D%7D%3B var options%3D%7Bpath:_p%7D%3B_i(_p%2B%27HTMLCS.js%27,function()%7BHTMLCSAuditor.run(%27WCAG2AA%27,null,options)%3B%7D)%3B%7D)()%3B

#### Axe
Axe is a really nice tool that adds an extra tab in your developer tools where
you can scan your page and get a list of potential issues.

The demo page for instance contains a color contrast issue in the notification
banner (white on the green background).

[You can download the Axe extension here](https://chrome.google.com/webstore/detail/axe-web-accessibility-tes/lhdoppojpmngadmnindnejefpokejbdd)
