/**
 *
 * Utility
 *
 */

window.__ERROR__ = false;

function later(fn, delay = 5000, ...args) {
  return new Promise(resolve => {
    setTimeout(() => {
      fn(...args);
      resolve();
    }, 5000);
  });
}

/**
 *
 * Main entry
 *
 */

function main() {
  const elements = {
    notifications: {
      polite: document.getElementById('js-notification-polite'),
      assertive: document.getElementById('js-notification-assertive'),
    },

    cookieBanner: document.getElementById('js-cookie-banner'),
    cookies: document.getElementById('js-accept-cookies'),
    form: {
      form: document.getElementById('js-newsletter-form'),
      submit: document.getElementById('js-newsletter-submit-button'),
      message: document.getElementById('js-newsletter-message'),
    },
  };

  function notify(element, message, error) {
    element.classList.toggle('notification--error', error);
    element.innerHTML = message;
    later(() => (element.innerHTML = ''));
  }

  const methods = {
    notify: {
      polite(message, error) {
        notify(elements.notifications.polite, message, error);
      },

      assertive(message, error) {
        notify(elements.notifications.assertive, message, error);
      },
    },
  };

  elements.form.form.addEventListener('submit', event => {
    event.preventDefault();

    elements.form.submit.classList.add('is-loading');
    elements.form.message.innerHTML = 'We are signing you up - hold on!';

    later(() => {
      elements.form.submit.classList.remove('is-loading');
      elements.form.message.innerHTML = '';

      methods.notify.assertive(
        window.__ERROR__
          ? 'Sadly something went wrong when we tried to sign you up.'
          : 'You successfully signed up for our delightful newsletter. Thank you!',
        window.__ERROR__
      );
    });
  });

  elements.cookies.addEventListener('click', event => {
    elements.cookieBanner.classList.add('is-hidden');
  });

  return methods;
}

/**
 *
 * Initialize
 *
 */

const app = main();

// later(app.notify.polite, 2000, "This is polite")
//   .then(() => later(app.notify.assertive, 8000, "This is assertive"))
//   .then(() => {
//     console.log("All notifications were run");
//   });
